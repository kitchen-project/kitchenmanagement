const connection = require('./db');
const express = require('express');

const createFlats = `CREATE TABLE IF NOT EXISTS FLATS (
    FLAT_ID INT AUTO_INCREMENT PRIMARY KEY,
    FLAT_NAME VARCHAR(50) NOT NULL,
    ADDRESS VARCHAR(100) NOT NULL
)`;

connection.query(createFlats, (err, results) => {
    if(err){
        console.error('error creating table', err);
    } else {
        console.log(' flats table created successfully');
    }
});


const createUsers = `CREATE TABLE IF NOT EXISTS USERS (
    USER_ID INT AUTO_INCREMENT PRIMARY KEY,
    USERNAME VARCHAR(20) UNIQUE NOT NULL,
    FIRST_NAME VARCHAR(20) NOT NULL,
    LAST_NAME VARCHAR(20) NOT NULL,
    MOBILE_NO VARCHAR(10) NOT NULL,
    EMAIL_ID VARCHAR(50) UNIQUE NOT NULL,
    PASSWORD VARCHAR(20) NOT NULL,
    USER_TYPE VARCHAR(10) NOT NULL,
    IS_FLAT_ADMIN BOOLEAN NOT NULL,
    FLAT_ID INT REFERENCES FLATS (FLAT_ID)
  )`;

connection.query(createUsers, (err, results) => {
    if(err){
        console.error('error creating table', err);
    } else {
        console.log('users table created successfully');
    }
});

const createItems = `CREATE TABLE IF NOT EXISTS ITEMS (
    ITEMS_ID INT AUTO_INCREMENT PRIMARY KEY,
    ITEM_NAME VARCHAR(50) NOT NULL,
    ITEM_UNIT VARCHAR(10) NOT NULL,
    ITEM_CATEGORY VARCHAR(20) NOT NULL
  )`;

  connection.query(createItems, (err, results) => {
    if(err){
        console.error('error creating table', err);
    } else {
        console.log('items table created successfully');
    }
});

const createAvl_Items = `CREATE TABLE IF NOT EXISTS AVAILABLE_ITEMS (
    AVLBL_ITEM_ID INT AUTO_INCREMENT PRIMARY KEY,
    FLAT_ID INT REFERENCES FLATS (FLAT_ID),
    ITEM_ID INT REFERENCES ITEMS (ITEMS_ID),
    QUANTITY INT NOT NULL,
    USER_ID INT REFERENCES USERS (USER_ID)
  )`;
  
  connection.query(createAvl_Items, (err, results) => {
    if(err){
        console.error('error creating table', err);
    } else {
        console.log('avl_items table created successfully');
    }
});

const createItemLog = `CREATE TABLE IF NOT EXISTS ITEM_LOG (
    AVLBL_ITEM_ID INT REFERENCES AVAILABLE_ITEMS (AVLBL_ITEM_ID),
    FLAT_ID INT REFERENCES FLATS (FLAT_ID),
    ITEM_ID INT REFERENCES ITEMS (ITEMS_ID),
    QUANTITY INT NOT NULL,
    PRICE DECIMAL(10,2) NOT NULL,
    UPDATE_DATE DATE NOT NULL,
    USER_ID INT REFERENCES USERS (USER_ID),
    PRIMARY KEY (AVLBL_ITEM_ID, UPDATE_DATE)
  )`;

  connection.query(createItemLog, (err, results) => {
    if(err){
        console.error('error creating table', err);
    } else {
        console.log('itemlog table created successfully');
    }
});

const createMenu =`CREATE TABLE IF NOT EXISTS MENU (
    MENU_ID INT AUTO_INCREMENT PRIMARY KEY,
    VEGETABLE_NAME VARCHAR(50) NOT NULL,
    MEAL_TIME VARCHAR(10) NOT NULL,
    AVLBL_ITEM_ID INT REFERENCES AVAILABLE_ITEMS (AVLBL_ITEM_ID),
    FLAT_ID INT REFERENCES FLATS (FLAT_ID)
  )`;

  connection.query(createMenu, (err, results) => {
    if(err){
        console.error('error creating table', err);
    } else {
        console.log('menu table created successfully');
    }
});

const createVoting = `CREATE TABLE IF NOT EXISTS VOTING (
  VOTING_ID INT AUTO_INCREMENT PRIMARY KEY,
  FLAT_ID INT REFERENCES FLATS (FLAT_ID),
  AVLBL_ITEM_ID INT REFERENCES AVAILABLE_ITEMS (AVLBL_ITEM_ID),
  USER_ID INT REFERENCES USERS (USER_ID)
)`;

connection.query(createVoting, (err, results) => {
    if(err){
        console.error('error creating table', err);
    } else {
        console.log('voting table created successfully');
    }
});

const createComments = `CREATE TABLE IF NOT EXISTS COMMENTS (
    COMMENT_ID INT AUTO_INCREMENT PRIMARY KEY,
    COMMENT TEXT NOT NULL,
    USER_ID INT REFERENCES USERS (USER_ID)
  )`;

  connection.query(createComments, (err, results) => {
    if(err){
        console.error('error creating table', err);
    } else {
        console.log('comments table created successfully');
    }
});
  