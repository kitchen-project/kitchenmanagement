const express =  require('express');
const parser =  require('body-parser');
const app = express();
const port = 3000;

const flats = require('./APIs/flats');
const users = require('./APIs/users');
const items = require('./APIs/items');
const avlitems = require('./APIs/avlitems');
const comments =  require('./APIs/comments');
const menu = require('./APIs/menu');
const voting =  require('./APIs/voting');


app.use('/flats', flats);
app.use('/users', users);
app.use('/items', items);
app.use('/avlitems', avlitems);
app.use('/comments', comments);
app.use('/menu', menu);
app.use('/voting', voting);

app.listen(3000, () => {
    console.log(`Server is running on port ${3000}`)});